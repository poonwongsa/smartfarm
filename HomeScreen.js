import React, { useState }from 'react';
import { TouchableOpacity, StyleSheet, Text, View, Image, FlatList } from 'react-native';

export default function HomeScreen() {
  
  const SOILMENU = [
    {
      id: '1',
      title: 'ความชื้นในดิน',
    },
    {
      id: '2',
      title: 'แร่ธาตุในดิน',
    },
    {
      id: '3',
      title: 'ความเป็นกรด-ด่าง (pH)',
    }
  ];

  const AIRMENU = [
    
    {
        id: '4',
        title: 'ความชื้นในอากาศ',
      },
      {
        id: '5',
        title: 'อุณหภูมิ',
      }
  ];

  const Item = ({ item, onPress, style }) => (
    <TouchableOpacity onPress={onPress} style={[styles.btnContainer, style]}>
      <Text style={styles.text}>{item.title}</Text>
    </TouchableOpacity>
  );

  const renderItem = ({ item }) => {
    return (
      <Item
        item={item}
        onPress={() => setSelectedId(item.id)}
        style={styles.button}
      />
    );
  };

  const [selectedId, setSelectedId] = useState(null);

  return (
    <View style={styles.container}>
        {/* Start Header Container */}
      <View style={styles.header}>
        <View style={{ width: "50%", margin: 35}}>
          <TouchableOpacity>
            <Image source={require('./assets/images/menu_24px.png')} />
          </TouchableOpacity>
        </View>
        <View style={{ width: "50%", margin: 35 }}>
          <TouchableOpacity>
            <Image source={require('./assets/images/noti-bell.png')} />
          </TouchableOpacity>
        </View>
      </View>
      {/* End Header Container */}


      {/* Start Body Container */}
      <View style={styles.body}>

         {/* Start soil menu */}
        <View style={styles.btnContainer}>
            <View style={{ flexDirection: 'row', marginVertical: 15}}>
              <View style={{ width: 40 }}>
                  <Image source={require('./assets/images/icon-sprout.png')}/>
              </View>
              <View>
                  <Text style={styles.text}> สภาพดิน </Text>
              </View>
            </View>

            <FlatList
              data={SOILMENU}
              renderItem={renderItem}
              keyExtractor={(item) => item.id}
              extraData={selectedId}
            />
        </View>
         {/* End soil menu */}

        {/* Start air menu */}
        <View style={styles.btnContainer}>
            <View style={{ flexDirection: 'row', marginVertical: 15}}>
                <View style={{ width: 40 }}>
                    <Image source={require('./assets/images/icon-sun.png')}/>
                </View>
                <View>
                    <Text style={styles.text}> สภาพอากาศ </Text>
                </View>
            </View>

            <FlatList
                data={AIRMENU}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
                extraData={selectedId}
            />
        </View>
         {/* End soil menu */}
      </View>
      {/* End Body Container */}

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2',
  },
  btnContainer: {
    paddingHorizontal: 30,
    marginVertical: 7
  },
  header: {
      height: "15%",
      backgroundColor: '#9DBC5C',
      flexDirection: 'row'
  },
  body: {
    height: "85%",
    backgroundColor: '#F2F2F2'
  },
  button: {
    alignItems: "center",
    backgroundColor: "#fff",
    padding: 20,
  },
  text: {
      fontSize: 18,
      
  }
});
